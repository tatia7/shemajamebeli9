package com.example.shemajamebeli9.Model

import android.widget.ImageView

data class courses (
    val title : String,
    val precent : Float,
    val image : ImageView,
    val color : String,
    val background_color_percent : Int
        )