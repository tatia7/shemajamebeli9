package com.example.shemajamebeli9.api

import com.example.shemajamebeli9.Model.mainModel
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepository {

    @GET("v3/29db8caa-95cb-44be-aa3c-eee0aa406870")
    suspend fun getList (): Response<mainModel>
}