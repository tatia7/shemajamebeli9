package com.example.shemajamebeli9.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebeli9.Model.courses
import com.example.shemajamebeli9.databinding.ItemView2Binding

class RecyclerAdapter() : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>(){

    private var course = mutableListOf<courses>()

    inner class ViewHolder(val binding : ItemView2Binding) : RecyclerView.ViewHolder(binding.root){
        fun bind(){
            val model = course[adapterPosition]
            binding.title.text = model.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemView2Binding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = course.size
}