package com.example.shemajamebeli9

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shemajamebeli9.Model.mainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ViewModel : ViewModel() {

    private val itemsLiveData = MutableLiveData<mainModel>().apply {
        mutableListOf<mainModel>()
    }

    val _itemsLiveData: MutableLiveData<mainModel> = itemsLiveData

    fun init() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
            }
        }

    }
}